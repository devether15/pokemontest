import React from 'react'
import {useState} from 'react'
import { useFetch } from '../hook/useFetch';

export const PokemonContext = React.createContext()

export const PokemonContextProvider = (props) => {
    const url = 'https://pokeapi.co/api/v2/pokemon'

    const [currentPage, setCurrentPage] = useState(url)
    const {data, loading, error, next, previous} = useFetch(currentPage)
    console.log(data);
    // console.log(next)



    const nextPage = () => {        
       setCurrentPage(next)
    }

    const previousPage = () => {        
        setCurrentPage(previous)
     }

    return(
        <PokemonContext.Provider value={{data, nextPage, previousPage, next, previous}}>
            {props.children}
        </PokemonContext.Provider>
    )
}


