import {useEffect, useState} from 'react'
import axios from 'axios'

export const useFetch = (url) => {

    const [state, setState] = useState({data: [],previous: null ,next: null, loading: true, error: ''})

    useEffect(() => {
    
        axios.get(url)
        
        .then(response =>{ 
            setState({ 
                data: response.data.results,
                previous: response.data.previous,
                next: response.data.next, 
                loading: false, error: null 
            })
         })
         
        .catch(() => {
            //...Error
        });
    }, [url]);
    return state
}
