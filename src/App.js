import './App.css'
import { PokemonContextProvider } from './context/PokemonContext';
import Header from './components/Header';

function App() {
  return (
    <PokemonContextProvider>
       <Header/>
    </PokemonContextProvider>    
  );
}

export default App;
